import sys
import os
import re
import operator


def avgclaculate(filename):
    times = {}
    hits = {}
    namepat = re.compile(r"[A-Z][a-z]+\s[A-Z][a-z]+")
    numpat = re.compile(r"\d+")
    titpat = re.compile(r"===.+===|#.*|^\s+$")
    f = open(filename, 'r')
    for line in f:
        match = titpat.match(line)
        # if the line is the title, comment or empty skip
        if match is not None:
            continue
        name = namepat.match(line).group(0)
        num = numpat.findall(line)
        if times.has_key(name):
            times[name] += int(num[0])
            hits[name] += int(num[1])
        else:
            times[name] = int(num[0])
            hits[name] = int(num[1])
    f.close()
    statistics = {}
    for key, value in hits.iteritems():
        statistics[key] = value * 1.0 / times[key]

    sortoutput = sorted(statistics.items(), key=operator.itemgetter(1), reverse=True)
    for element in sortoutput:
        print("{0}: {1:.3f}".format(element[0], element[1]))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit("Usage: %s needs to run with a file argument" % sys.argv[0])

    filename = sys.argv[1]

    if not os.path.exists(filename):
        sys.exit("Error: File '%s' not found" % sys.argv[1])
    avgclaculate(filename)